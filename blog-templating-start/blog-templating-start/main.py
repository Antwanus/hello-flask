from post import Post
import requests
from flask import Flask, render_template

app = Flask(__name__)

response_json = requests.get('https://api.npoint.io/c790b4d5cab58020d391').json()
all_posts = [Post(e['id'], e['title'], e['subtitle'], e['body']) for e in response_json]

def find_post_by_id(post_id: int) -> Post:
    for p in all_posts:
        if p.id == post_id:
            return p


@app.route('/')
def home():
    return render_template("index.html", all_posts=all_posts)


@app.route('/post/<int:post_id>')
def show_post(post_id):
    return render_template('post.html', post=find_post_by_id(post_id))


if __name__ == "__main__":
    app.run(debug=True)
