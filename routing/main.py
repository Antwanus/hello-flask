from flask import Flask


app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<h1 style="text-align: center">Greetings, world</h1>' \
           '<p>What\'s going on?<p>' \
           '<img src="https://media.giphy.com/media/Wn74RUT0vjnoU98Hnt/giphy.gif">'

def add_style_underline_decorator(input_fn):
    def inner_fn():
        return '<u>' + input_fn() + '</u>'
    return inner_fn
def add_style_emphasize_decorator(input_fn):
    def inner_fn():
        return '<em>' + input_fn() + '</em>'
    return inner_fn
def add_style_bold_decorator(input_fn):
    def inner_fn():
        return '<b>' + input_fn() + '</b>'
    return inner_fn

@app.route('/bye')
@add_style_underline_decorator
@add_style_emphasize_decorator
@add_style_bold_decorator
def goodbye_world():
    return 'Take care now, byebye then'

# @app.route(f'/user/<int:name>')   # Casts name to an integer
@app.route(f'/user/<name>/home')    # Default string
def greet(name):
    return f'<h1>Welcome home user {name}.</h1>'

@app.route(f'/user/<path:subpath>')
def greet_every_user_subpath(subpath):
    return f'<h1>Welcome home subpath {subpath}.</h1>'


if __name__ == "__main__":
    app.run(debug=True)
