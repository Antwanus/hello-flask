import requests
from flask import Flask, render_template
import random
import datetime as dt

GENDERIZE_URL = 'https://api.genderize.io?name='
AGIFY_URL = 'https://api.agify.io?name='

app = Flask(__name__)

@app.route('/')
def index():
    random_number = random.randint(1, 10)
    this_year = dt.datetime.now().year
    return render_template(template_name_or_list='index.html', num=random_number, copyright_year=this_year)

@app.route('/guess/<name>')
def guess(name: str):
    gender_response = requests.get(f'{GENDERIZE_URL}{name}')
    gender = gender_response.json()['gender']
    age_response = requests.get(f'{AGIFY_URL}{name}')
    age = age_response.json()['age']
    name_capitalized = name.title()
    return render_template('guess.html', gender=gender, age=age, name=name_capitalized)

@app.route('/blog')
def blog():
    blog_list = requests.get('https://api.npoint.io/c790b4d5cab58020d391').json()
    return render_template('blog.html', posts=blog_list)

@app.route("/blog/<int:num>")
def get_blog(num):
    print(num)
    response = requests.get('https://api.npoint.io/c790b4d5cab58020d391')
    all_posts = response.json()
    return render_template("blog.html", posts=all_posts)


if __name__ == "__main__":
    app.run(debug=True)
