# ********Day 54 Start**********
def add(n1, n2):
    return n1 + n2


def subtract(n1, n2):
    return n1 - n2


def multiply(n1, n2):
    return n1 * n2


def divide(n1, n2):
    return n1 / n2


# Functions are first-class objects, which can be passed around as arguments just like int/string/...
def calculate(calculation_function, n1, n2):
    return calculation_function(n1, n2)


result = calculate(add, 2, 3)
result1 = calculate(multiply, add(1, 2), calculate(divide, 1, 2))
print(f'calculate(add, 2, 3) = {result}\ncalculate(multiply, add(1, 2), calculate(divide, 1, 2)) = {result1}')


# Functions can be nested in other decorators

def outer_function():
    print("I'm outer")

    def nested_function():
        print("I'm inner")

    nested_function()


outer_function()


# Functions can be returned from other decorators
def outer_function():
    print("I'm outer")

    def nested_function():
        print("I'm inner")

    return nested_function


inner_function = outer_function()
inner_function()

# Simple Python Decorator Functions
import time


def decorator_function(input_function):
    def the_inner_function_name():
        print('Decorator sleepy, brb 2 secs')
        time.sleep(secs=2)
        # Do something before
        input_function()
        input_function()
        # Do something after

    return the_inner_function_name

@decorator_function
def say_hello():
    print("Hello")


# With the @ syntactic sugar
@decorator_function
def say_bye():
    print("Bye")


# Without the @ syntactic sugar
def say_greeting():
    print("How are you?")


say_hello()
say_bye()

decorated_function = decorator_function(say_greeting)
decorated_function()
