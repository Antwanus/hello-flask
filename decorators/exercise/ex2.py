def logging_decorator(fn):
    def i_fn(*args, **kwargs):
        print(f"You called {fn.__name__}{args}")
        result = fn(args[0], args[1], args[2])
        print(f"It returned: {result}")

    return i_fn


@logging_decorator
def a_function(number0, number1, number2):
    return number0 * number1 * number2


a_function(1, 2, 3)
