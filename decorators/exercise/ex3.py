# Advanced Python Decorator Functions
class User:
    def __init__(self, name):
        self.name = name
        self.is_logged_in = False

def is_authenticated_decorator(input_function):
    def wrapper(*args, **kwargs):
        user: User = args[0]
        if user.is_logged_in:
            input_function(user)
        elif not user.is_logged_in:
            print(user.name, 'is not logged in')
    return wrapper

@is_authenticated_decorator
def create_blog_post(user):
    print(f"This is {user.name}'s new blog post.")


new_user = User("angela")
create_blog_post(new_user)
new_user.is_logged_in = True
create_blog_post(new_user)
