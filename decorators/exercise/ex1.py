import time

def speed_calc_decorator(input_function):
    def stopwatch_f():
        print(f'Checking timelapse for <{input_function.__name__}>...')
        start = time.time()
        input_function()
        result = time.time() - start
        print(f'Time elapsed <{input_function.__name__}> = {result} seconds!')
        print(result)

    return stopwatch_f

@speed_calc_decorator
def fast_function():
    for i in range(10000000):
        i * i

@speed_calc_decorator
def slow_function():
    for i in range(100000000):
        i * i


fast_function()
slow_function()
