from flask import Flask
import random


app = Flask(__name__)
random_number = random.randint(1, 11)
print(random_number)


@app.route('/')
def index():
    return '<h1>Guess a number between 0 and 9</h1>' \
           '<img src="https://media.giphy.com/media/Sl1Z1pNi0sl4k/giphy.gif">'

@app.route(f'/{random_number}')
def secret_number():
    return '<h1>You guessed the number!</h1>' \
           '<img src="https://media.giphy.com/media/4T7e4DmcrP9du/giphy.gif">'

@app.route('/<int:guess>')
def wrong_guess(guess):
    msg = '<h1>You guessed wrong!</h1>'
    if guess > random_number:
        msg += '<h3>Too high</h3>' \
               '<img src="https://media.giphy.com/media/3o6ZtaO9BZHcOjmErm/giphy.gif">'
    elif guess < random_number:
        msg += '<h3>Too low</h3' \
               '<img src="https://media.giphy.com/media/jD4DwBtqPXRXa/giphy.gif">'
    return msg


if __name__ == "__main__":
    app.run()
