from flask import Flask

app = Flask(__name__)
# print(__name__)         # CLI:> __main__
# print(random.__name__)  # CLI:> random

@app.route('/')
def hello_world():
    return '<h1>Greetings, world</h1>'

@app.route('/bye')
def goodbye_world():
    return '<h1>Take care now, byebye then</h1>'


if __name__ == "__main__":
    app.run()
